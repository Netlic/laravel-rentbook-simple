<?php

namespace Bookrent\Management\Controllers;

use Bookrent\Common\Models\Author;
use Bookrent\Http\Controllers\Controller;
use Bookrent\Http\Requests\AuthorRequest;
use Bookrent\Services\AuthorService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class AuthorController extends Controller
{
    public function add(AuthorRequest $request)
    {
        if ($request->validated()) {
            try {
                /** @var Author $author */
                $author = Author::create([
                    Author::NAME => ucfirst($request->name),
                    Author::SURNAME => ucfirst($request->surname)
                ]);
                return Response::json([
                    'message' => sprintf(__('database.save.author.success'), $author->fullName)
                ], 200);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                return Response::json([
                    'message' => __('database.save.author.error')
                ], 500);
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(Request $request)
    {
        /** @var Collection $authors */
        $authors = Author::withCount('books')->get();
        if ($request->ajax()) {
            return Response::json($authors->toArray());
        }
    }

    public function listByName(string $name, Request $request, AuthorService $authorService)
    {
        $authors = $authorService->listAuthorsByName(trim($name));

        if ($request->ajax()) {
            return Response::json($authors->toArray());
        }

        abort(404);
    }

    public function renderForm(int $id = null)
    {
        if ($id) {
            $author = Author::find($id);
            if (!$author) {
                Log::error('Author not found: ' . $id);
                abort(404);
            }
            return view('management.author.update', ['author' => $author]);
        }
        return view('management.author.add');
    }

    public function update(AuthorRequest $request)
    {
        try {
            Author::find($request->id)->update(
                [
                    Author::NAME => $request->name,
                    Author::SURNAME => $request->surname,
                    Author::UPDATED_AT => Carbon::now()->toDateTimeString()
                ]
            );
            return Response::json([
                'message' => sprintf(__('database.update.author.success'), "{$request->name} {$request->surname}")
            ], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return Response::json([
                'message' => __('database.save.author.error')
            ], 500);
        }
    }
}
