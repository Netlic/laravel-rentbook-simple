<?php

namespace Bookrent\Management\Controllers;

use Bookrent\Common\Models\Book;
use Bookrent\Http\Controllers\Controller;

class ManagementController extends Controller
{
    public function __invoke()
    {
        return view('management.index', ['books' => Book::with('author')->get()]);
    }
}
