<?php

namespace Bookrent\Management\Controllers;

use Bookrent\Common\Models\BorrowedBook;
use Bookrent\Common\Models\Book;
use Bookrent\Common\Models\BookCovers;
use Bookrent\Http\Requests\BookRequest;
use Bookrent\Services\BookService;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class BookController
{
    /**
     * @param BookService $bookService
     * @param BookRequest $request
     * @return JsonResponse
     */
    public function add(BookService $bookService, BookRequest $request)
    {
        if ($request->validated()) {
            try {
                $book = $bookService->saveBook($request);
                return Response::json([
                    'message' => sprintf(__('database.save.book.success'), $book->title)
                ], 200);
            } catch (\PDOException $e) {
                Log::error($e->getMessage());
                return Response::json([
                    'message' => __('database.save.book.error')
                ], 500);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                return Response::json([
                    'message' => __('database.save.book.error')
                ], 500);
            }
        }
    }

    /**
     * @param int|null $id
     * @return Application|Factory|View
     */
    public function renderForm(int $id = null)
    {
        if ($id) {
            $book = Book::with('author')->with('covers')->find($id);
            if (!$book) {
                Log::error('Book not found: ' . $id);
                abort(404);
            }
            return view('management.books.update', ['book' => $book]);
        }
        return view('management.books.add');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        /** @var Collection $authors */
        $books = Book::get();
        if ($request->ajax()) {
            return Response::json($books->toArray());
        }
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function borrow(int $id)
    {
        /** @var Book $book */
        $book = Book::find($id);
        if (!$book) {
            abort(404);
        }

        if ($book->status) {
            try {
                BorrowedBook::create([
                    BorrowedBook::BOOK_ID => $book->id
                ]);
                return Response::json([
                    'message' => sprintf(__('database.borrow.book.success'), $book->title)
                ], 200);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                return Response::json([
                    'message' => __('database.borrow.book.error')
                ], 500);
            }
        }

        return Response::json([
            'message' => sprintf(__('database.borrow.book.unavailable'), $book->title)
        ], 500);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id)
    {
        /** @var Book $book */
        $book = Book::find($id);
        try {
            $book->delete();
            return Response::json([
                'message' => sprintf(__('database.delete.book.success'), $book->title)
            ], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return Response::json([
                'message' => __('database.delete.book.error')
            ], 500);
        }
    }

    /**
     * @param BookService $bookService
     * @param BookRequest $request
     * @return JsonResponse
     */
    public function update(BookService $bookService, BookRequest $request)
    {
        if ($request->validated()) {
            try {
                $book = $bookService->update($request);
                return Response::json([
                    'message' => sprintf(__('database.update.book.success'), $book[Book::TITLE])
                ], 200);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
                return Response::json([
                    'message' => __('database.save.book.error')
                ], 500);
            }
        }
    }

    /**
     * @param int $bookId
     * @param int $id
     */
    public function deleteCover(int $bookId, int $id)
    {
        /** @var BookCovers $cover */
        $cover = Book::find($bookId)->covers->only($id)->first;
        if (!$cover) {
            abort(404);
        }

        try {
            $cover->delete();
            return Response::json([
                'message' => __('database.update.cover.success')
            ], 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return Response::json([
                'message' => __('database.delete.cover.error')
            ], 500);
        }
    }

    public function getBorrowData(int $id)
    {
        /** @var Book $book */
        $book = Book::find($id);
        if (!$book) {
            abort(404);
        }

        return Response::json($book->borrowings->toArray(), 200);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function return(int $id)
    {
        /** @var Book $book */
        $book = Book::find($id);

        if (!$book) {
            abort(404);
        }

        try {
            /** @var BorrowedBook $borrowing */
            $borrowing = $book->borrowed_books->random();
            $borrowing->update([
                BorrowedBook::RETURNED_AT => Carbon::now()->toDateTimeString()
            ]);
            return Response::json([
                'message' => sprintf(__('database.return.book.success'), $book->title)
            ], 200);
        } catch (\PDOException $e) {
            Log::error($e->getMessage());
            return Response::json([
                'message' => __('database.return.book.error')
            ], 500);
        } catch (\Exception $e) {
            return Response::json([
                'message' => sprintf(__('database.return.book.returned'))
            ], 500);
        }
    }
}
