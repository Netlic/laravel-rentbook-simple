<?php

namespace Bookrent\Common\Controllers;

use Bookrent\Common\Models\Book;
use Bookrent\Common\Models\BookCovers;
use Bookrent\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;


class ImageController extends Controller
{
    public function getBookCover(int $id, int $fileId)
    {
        /** @var Book $book */
        $book = Book::find($id);
        if ($book) {
            $fileCollection = $book->covers->only([$fileId]);
            if ($fileCollection->count() > 0) {
                /** @var BookCovers $file */
                $file = $fileCollection->first();

                return Response::file($file->path);
            }
        }

        abort(404);
    }
}
