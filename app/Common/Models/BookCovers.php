<?php

namespace Bookrent\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class BookCovers
 * @package Bookrent\Common\Models
 * @property int $book_id
 * @property Book $book
 * @property string $path
 * @property int $size
 * @property string $name
 */
class BookCovers extends Model
{
    use HasFactory;

    public const PATH = 'path';
    public const BOOK_ID = 'book_id';

    /** @var string */
    protected $table = 'book_covers';

    /** @var string[] */
    protected $fillable = [
        'book_id',
        'path'
    ];

    protected $appends = ['name', 'size', 'url', 'type'];

    /**
     * @return BelongsTo
     */
    public function book() :BelongsTo
    {
        return $this->belongsTo(Book::class);
    }

    /**
     * @return string
     */
    public function getPathAttribute() :string
    {
        return public_path($this->attributes[self::PATH]);
    }

    /**
     * @return string
     */
    public function getNameAttribute(): string
    {
        $pathArray = explode(DIRECTORY_SEPARATOR, $this->path);
        return array_pop($pathArray);
    }

    /**
     * @return string
     */
    public function getSizeAttribute(): string
    {
        return filesize($this->path);
    }

    public function getUrlAttribute(): string
    {
        return sprintf('/bookcover/%d/%d', $this->book->id, $this->id);
    }

    /**
     * Gets the mime type of the file
     * @return string
     */
    public function getTypeAttribute() :string
    {
        return mime_content_type($this->path);
    }

    /**
     * @return bool|void|null
     * @throws \Exception
     */
    public function delete()
    {
        if (parent::delete()) {
            $file = $this->path;
            if (file_exists($file)) {
                if (!unlink($file)) {
                    throw new \Exception('Remove file not successful');
                }
            }
            return true;
        }
        return false;
    }
}
