<?php

namespace Bookrent\Common\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Book
 * @package Bookrent\Common\Models
 * @property int $id
 * @property string $title
 * @property int $quantity
 * @property Collection $covers
 * @property string $status
 * @property Collection $borrowings
 * @property int $borrowed_count
 * @property Collection $borrowed_books
 */
class Book extends Model
{
    use HasFactory;

    public const TITLE = 'title';
    public const QUANTITY = 'quantity';
    public const AUTHOR_ID = 'author_id';

    protected $table = 'books';

    protected $fillable = [
        'title', 'quantity', 'author_id'
    ];

    protected $appends = ['status', 'available_quantity'];

    /**
     * @return bool
     */
    public function getStatusAttribute() :bool
    {
        return $this->available_quantity > 0;
    }

    /**
     * @return int|mixed
     */
    public function getAvailableQuantityAttribute() :int
    {
        return $this->quantity - $this->borrowed_count;
    }

    /**
     * @return BelongsTo
     */
    public function author() :BelongsTo
    {
        return $this->belongsTo(Author::class);
    }

    /**
     * @return HasMany
     */
    public function covers() :HasMany
    {
        return $this->hasMany(BookCovers::class);
    }

    /**
     * @return HasMany
     */
    public function borrowings() :HasMany
    {
        return $this->hasMany(BorrowedBook::class);
    }

    /**
     * @return int
     */
    public function getBorrowedCountAttribute() :int
    {
        return count($this->borrowed_books);
    }

    /**
     * @return Collection
     */
    public function getBorrowedBooksAttribute() :Collection
    {
        return $this->borrowings->filter(function($value) {
            return empty($value->returned_at);
        });
    }
}
