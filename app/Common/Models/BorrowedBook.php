<?php

namespace Bookrent\Common\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BorrowedBook extends Model
{
    use HasFactory;

    public const BOOK_ID = 'book_id';
    public const BORROWED_AT = 'borrowed_at';
    public const RETURNED_AT = 'returned_at';

    protected $fillable = [
        self::BOOK_ID, self::BORROWED_AT, self::RETURNED_AT
    ];

    public $timestamps = false;
}
