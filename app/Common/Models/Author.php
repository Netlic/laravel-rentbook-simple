<?php

namespace Bookrent\Common\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Author
 * @package Bookrent\Models
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $fullName
 * @property Collection $books
 */
class Author extends Model
{
    use HasFactory;

    public const NAME = 'name';
    public const SURNAME = 'surname';

    protected $table = 'authors';

    protected $fillable = [
        'name', 'surname'
    ];

    protected $appends = ['full_name'];

    /**
     * @param string $fullName
     */
    public function setFullNameAttribute(string $fullName)
    {
        $fullName = explode(' ', ucwords($fullName), 2);
        if (count($fullName) === 1) {
            $fullName[] = '';
        }

        $authorData = array_combine([Author::NAME, Author::SURNAME], $fullName);
        $this->attributes = array_merge($this->attributes, $authorData);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucwords("{$this->name} {$this->surname}");
    }

    /**
     * @return HasMany
     */
    public function books() :HasMany
    {
        return $this->hasMany(Book::class);
    }
}
