<?php

namespace Bookrent\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2|max:255'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => __('validations.book.title.required'),
            'title.max' => __('validations.book.title.max'),
            'title.min' => __('validations.book.title.min'),
        ];
    }
}
