<?php

namespace Bookrent\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:255',
            'surname' => 'required|min:2|max:255'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __('validations.author.name.required'),
            'name.max' => __('validations.author.name.max'),
            'name.min' => __('validations.author.name.min'),
            'surname.required' => __('validations.author.surname.required'),
            'surname.max' => __('validations.author.surname.max'),
            'surname.min' => __('validations.author.surname.min'),
        ];
    }
}
