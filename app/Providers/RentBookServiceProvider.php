<?php

namespace Bookrent\Providers;

use Bookrent\Services\AuthorService;
use Bookrent\Services\BookCoverService;
use Bookrent\Services\BookService;
use Illuminate\Support\ServiceProvider;

class RentBookServiceProvider extends ServiceProvider
{
    public function register() {
        $this->app->singleton(AuthorService::class, function () {
            return new AuthorService();
        });
        $this->app->singleton(BookService::class, function () {
            return new BookService();
        });
        $this->app->singleton(BookCoverService::class, function () {
            return new BookCoverService();
        });
    }
}
