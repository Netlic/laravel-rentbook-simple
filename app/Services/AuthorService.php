<?php

namespace Bookrent\Services;

use Bookrent\Common\Models\Author;
use Illuminate\Database\Eloquent\Collection;

class AuthorService
{
    /**
     * @param string $name
     * @return Collection
     */
    public function listAuthorsByName(string $name) :Collection
    {
        return Author::where('name', 'like', "%{$name}%")
            ->orWhere('surname', 'like', "%{$name}%")
            ->get();
    }
}
