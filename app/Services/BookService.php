<?php

namespace Bookrent\Services;

use Bookrent\Common\Models\Author;
use Bookrent\Common\Models\Book;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookService
{
    /**
     * @param Request $request
     * @return Book|null
     */
    public function saveBook(Request $request) :?Book
    {
        $book = null;
        DB::transaction(function() use ($request, &$book) {
            $bookData = [];
            $this->populateBookData($request, $bookData);

            $book = Book::create($bookData);

            if (!empty($request->file('files'))) {
                app(BookCoverService::class)->saveBookCovers($book->id, $request->file('files'));
            }
        });

        return $book;
    }

    /**
     * @param Request $request
     * @return array|null
     */
    public function update(Request $request) :?array
    {
        $book = null;
        DB::transaction(function() use ($request, &$book) {
            $bookData = [];
            $this->populateBookData($request, $bookData);
            $bookData[Book::UPDATED_AT] = Carbon::now()->toDateTimeString();

            Book::find($request->id)->update(
                $bookData
            );
            $book = $bookData;
            if (!empty($request->file('files'))) {
                app(BookCoverService::class)->saveBookCovers($request->id, $request->file('files'));
            }
        });

        return $book;
    }

    /**
     * @param Request $request
     * @param array $bookData
     */
    private function populateBookData(Request $request, array &$bookData)
    {
        $bookData = [
            Book::TITLE => $request->title,
            Book::QUANTITY => (int)$request->quantity
        ];

        if (!empty($request->author_id)) {
            $bookData[Book::AUTHOR_ID] = $request->author_id;
        } elseif (!empty($request->author)) {
            $author = Author::create($this->createAuthorNameData($request->author));
            $bookData[Book::AUTHOR_ID] = $author->id;
        }
    }

    private function createAuthorNameData(string $authorName)
    {
        $fullName = explode(' ', ucwords($authorName), 2);
        if (count($fullName) === 1) {
            $fullName[] = '';
        }

        return array_combine([Author::NAME, Author::SURNAME], $fullName);
    }
}
