<?php

namespace Bookrent\Services;

use Bookrent\Common\Models\BookCovers;
use Illuminate\Http\UploadedFile;

class BookCoverService
{
    /**
     * @param int $bookId
     * @param array $files
     */
    public function saveBookCovers(int $bookId, array $files)
    {
        $path = sprintf('book_covers/book_%s/', $bookId);
        $name = 'cover';

        /** @var UploadedFile $file */
        foreach ($files as $index => $file) {
            BookCovers::create([
                BookCovers::PATH => $path . $name . $index,
                BookCovers::BOOK_ID => $bookId
            ]);
            $file->move(public_path($path), $name . $index);
        }
    }
}
