<!DOCTYPE html>
<html lang="sk">
    <head>
        <title>Bookrent - @yield('title')</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @yield('head_components')
    </head>
    <body>
        <div id="app">
            @yield('page_logo_header')

            @section('breadcrumbs')
            @show

            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <script src="{!! asset('js/app.js') !!}" ></script>
    </body>
