@extends('layouts.management')

@section('title', __('titles.management.book.update'))
@section('breadcrumbs')
    <breadcrumbs v-bind:skip-parts="['last']"></breadcrumbs>
@endsection
@section('content')
    <book-form v-bind:book="{{ $book->toJson() }}"></book-form>
@endsection
