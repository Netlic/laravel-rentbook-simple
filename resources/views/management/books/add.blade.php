@extends('layouts.management')

@section('title', __('titles.management.book.add'))
@section('breadcrumbs')
    <breadcrumbs></breadcrumbs>
@endsection
@section('content')
    <book-form></book-form>
@endsection
