@extends('layouts.management')

@section('title', __('titles.management.book.overview'))
@section('breadcrumbs')
    <breadcrumbs></breadcrumbs>
@endsection
@section('content')
    <list-records
        new-entity-where-to="book"
        no-entity-alert-message="{{ __('custom.management.alerts.no-books') }}"
        v-bind:list-columns="['title', 'available_quantity', 'status']"
        v-bind:additional-actions="['borrow', 'return']"
        add-button-text="{{ __('custom.button.book') }}"
        v-bind:translations="{status: {
            'true': 'custom.status.available',
            'false': 'custom.status.borrowed'
        }}"
    ></list-records>
@endsection
