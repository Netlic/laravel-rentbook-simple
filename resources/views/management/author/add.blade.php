@extends('layouts.management')

@section('title', __('titles.management.author.add'))
@section('breadcrumbs')
    <breadcrumbs></breadcrumbs>
@endsection
@section('content')
    <author-form></author-form>
@endsection
