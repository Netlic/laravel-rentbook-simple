@extends('layouts.management')

@section('title', __('titles.management.author.update'))
@section('breadcrumbs')
    <breadcrumbs v-bind:skip-parts="['last']"></breadcrumbs>
@endsection
@section('content')
    <author-form v-bind:author="{{ $author->toJson() }}"></author-form>
@endsection
