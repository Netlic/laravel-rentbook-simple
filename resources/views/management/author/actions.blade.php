@extends('layouts.management')

@section('title', __('titles.management.author.overview'))
@section('breadcrumbs')
    <breadcrumbs></breadcrumbs>
@endsection
@section('content')
    <list-records
        new-entity-where-to="author"
        no-entity-alert-message="{{ __('custom.management.alerts.no-authors') }}"
        v-bind:list-columns="['name', 'surname', 'books_count']"
        add-button-text="{{ __('custom.button.author') }}"
    ></list-records>
@endsection
