@extends('layouts.management')

@section('title', __('titles.management.index'))

@section('content')
    <management-index v-bind:books="{{ $books->toJson() }}"></management-index>
@endsection
