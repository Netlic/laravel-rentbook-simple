<?php

return [
    'management' => [
        'index' => 'Manažérsky prehľad',
        'book' => [
            'add' => 'Pridať knihu',
            'overview' => 'Prehľad kníh',
            'update' => 'Úprava knihy'
        ],
        'author' => [
            'add' => 'Pridať autora',
            'overview' => 'Prehľad autorov',
            'update' => 'Úprava autora'
        ]
    ]
];
