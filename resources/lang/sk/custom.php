<?php

return [
    'management' => [
        'header' => 'manažment',
        'alerts' => [
            'no-authors' => 'Žiadni autori neboli nájdení',
            'no-books' => 'Nevyskytujú sa tu žiadne tituly'
        ]
    ],
    'list' => [
        'table-headers' => [
            'title' => 'Názov',
            'quantity' => 'Počet',
            'name' => 'Meno',
            'surname' => 'Priezvisko',
            'created' => 'Vytvorený',
            'updated' => 'Zmenený',
            'options' => 'Možnosti',
            'author' => 'Autor',
            'status' => 'Stav',
            'books_count' => 'Počet publikácií',
            'borrowed' => 'Požičané',
            'returned' => 'Vrátené',
            'available_quantity' => 'Dostupné množstvo'
        ],
        'action-buttons' => [
            'edit-details' => 'Upraviť detaily',
            'delete' => 'Vymazať',
            'borrow' => 'Požičať',
            'show-borrowed' => 'Ukázať požičané',
            'return' => 'Vrátiť'
        ]
    ],
    'button' => [
        'author' => 'Autor',
        'book' => 'Kniha'
    ],
    'confirm' => [
        'header' => 'Prosím potvrďte nasledovné',
        'yes' => 'Áno',
        'storno' => 'Zrušiť',
        'ok' => 'Beriem na vedomie',
        'book' => [
            'remove' => 'Naozaj vymazať knihu?'
        ],
        'record' => [
            'remove' => 'Naozaj vymazať záznam?'
        ]
    ],
    'autocomplete' => [
        'no-suggestions' => 'Žiadne návrhy'
    ],
    'form' => [
        'labels' => [
            'name' => 'Meno',
            'surname' => 'Priezvisko',
            'submit' => 'Uložiť',
            'title' => 'Názov',
            'quantity' => 'Počet',
            'covers' => 'Obaly knihy',
            'label_author' => 'Autor',
            'upload-text' => 'Potiahnite obrázok(y) sem',
            'books_count' => 'Počet publikácií',
            'status' => 'Stav',
            'available_quantity' => 'Dostupné množstvo',
            'author' => [
                'full_name' => 'Meno autora'
            ]
        ]
    ],
    'breadcrumbs' => [
        'Management' => 'Manažment',
        'Add' => 'Pridať',
        'Update' => 'Upraviť',
        'Book' => 'Kniha',
        'Author' => 'Autor',
    ],
    'no-result' => 'Požičovňa je prázdna. Prosím pridajte knihy alebo autorov.',
    'status' => [
        'available' => 'Dostupné',
        'borrowed' => 'Vypožičané',

    ],
    'filter-bar' => [
        'filter-label' => 'Filtrovať'
    ]

];
