<?php

return [
    'save' => [
        'book' => [
            'success' => 'Knihu "%s" sa podarilo uložiť',
            'error' => 'Chyba pri ukladaní knihy'
        ],
        'author' => [
            'success' => 'Autor "%s" bol pridaný',
            'error' => 'Chyba pri ukladaní autora'
        ]
    ],
    'update' => [
        'author' => [
            'success' => 'Autor "%s" bol upravený',
        ],
        'book' => [
            'success' => 'Kniha "%s" bola upravená',
        ],
    ],
    'borrow' => [
        'book' => [
            'success' => 'Kniha "%s" bola požičaná',
            'error' => 'Chyba pri požičiavaní knihy',
            'unavailable' => 'Všetky kopie knihy sú už požičané'
        ],
    ],
    'return' => [
        'book' => [
            'success' => 'Kniha "%s" bola vrátená',
            'error' => 'Chyba pri vracaní knihy',
            'returned' => 'Všetky kópie knihy sú už vrátené'
        ],
    ],
    'delete' => [
        'author' => [
            'success' => 'Autor "%s" bol vymazaný',
            'error' => 'Autora sa nepodarilo vymazať'
        ],
        'book' => [
            'success' => 'Kniha "%s" bola vymazaná',
            'error' => 'Knihu sa nepodarilo vymazať'
        ],
        'cover' => [
            'success' => 'Obal knihy bol vymazaná',
            'error' => 'Knihu sa nepodarilo vymazať'
        ]
    ],
];
