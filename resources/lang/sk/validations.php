<?php

return [
    'book' => [
        'title' => [
            'required' => 'Názov knihy je potrebné zadať',
            'min' => 'Názov knihy musí mať aspoň 2 znaky',
            'max' => 'Príliš dlhý názov knihy'
        ]
    ],

    'author' => [
        'name' => [
            'required' => 'Meno autora je potrebné zadať',
            'min' => 'Meno autora musí mať aspoň 2 znaky',
            'max' => 'Príliš dlhé meno autora'
        ],
        'surname' => [
            'required' => 'Priezvisko je potrebné zadať',
            'min' => 'Priezvisko musí mať aspoň 2 znaky',
            'max' => 'Príliš dlhé Priezvisko'
        ],
    ]
];
