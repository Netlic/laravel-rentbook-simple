<?php

return [
    'book' => [
        'title' => [
            'required' => 'The book title is required',
            'min' => 'The book title must be at least 2 characters long',
            'max' => 'The book title is too long'
        ]
    ],

    'author' => [
        'name' => [
            'required' => 'The author name is mandatory',
            'min' => 'The author name must be at least 2 characters long',
            'max' => 'The author name is too long'
        ],
        'surname' => [
            'required' => 'The author surname is mandatory',
            'min' => 'The author surname must be at least 2 characters long',
            'max' => 'The author surname is too long'
        ],
    ]
];
