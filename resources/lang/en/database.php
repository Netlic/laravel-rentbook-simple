<?php

return [
    'save' => [
        'book' => [
            'success' => 'The book "%s" has been saved',
            'error' => 'Error during book saving'
        ],
        'author' => [
            'success' => 'Author "%s" has been added',
            'error' => 'Error during author saving'
        ]
    ],
    'update' => [
        'author' => [
            'success' => 'Author "%s" has been updated',
        ],
        'book' => [
            'success' => 'The book "%s" has been updated',
        ],
    ],
    'borrow' => [
        'book' => [
            'success' => 'The book "%s" has been borrowed',
            'error' => 'Error during book borrowing',
            'unavailable' => 'All the book copies are borrowed'
        ],
    ],
    'return' => [
        'book' => [
            'success' => 'The book "%s" has been returned',
            'error' => 'Return error',
            'returned' => 'All the book copies are returned'
        ],
    ],
    'delete' => [
        'author' => [
            'success' => 'Author "%s" has been deleted',
            'error' => 'Unable to delete the author'
        ],
        'book' => [
            'success' => 'The book "%s" has been deleted',
            'error' => 'Unable to delete the book'
        ],
        'cover' => [
            'success' => 'The book cover has been deleted',
            'error' => 'Unable to delete the book cover'
        ]
    ],
];
