<?php

return [
    'management' => [
        'index' => 'Manager overview',
        'book' => [
            'add' => 'Add book',
            'overview' => 'Book overview',
            'update' => 'Book Updated'
        ],
        'author' => [
            'add' => 'Add author',
            'overview' => 'Author overview',
            'update' => 'Author update'
        ]
    ]
];
