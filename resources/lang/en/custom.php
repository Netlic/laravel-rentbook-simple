<?php

return [
    'management' => [
        'header' => 'management',
        'alerts' => [
            'no-authors' => 'No authors found',
            'no-books' => 'No books available'
        ]
    ],
    'list' => [
        'table-headers' => [
            'title' => 'Title',
            'quantity' => 'Quantity',
            'name' => 'Name',
            'surname' => 'Surname',
            'created' => 'Created',
            'updated' => 'Updated',
            'options' => 'Options',
            'author' => 'Author',
            'status' => 'Status',
            'books_count' => 'Publications count',
            'borrowed' => 'Borrowed',
            'returned' => 'Returned',
            'available_quantity' => 'Available quantity'
        ],
        'action-buttons' => [
            'edit-details' => 'Edit details',
            'delete' => 'Delete',
            'borrow' => 'Borrow',
            'show-borrowed' => 'Show borrowed',
            'return' => 'Return'
        ]
    ],
    'button' => [
        'author' => 'Author',
        'book' => 'Book'
    ],
    'confirm' => [
        'header' => 'Please confirm the following',
        'yes' => 'Yes',
        'storno' => 'Cancel',
        'ok' => 'Okay',
        'book' => [
            'remove' => 'Really delete book?'
        ],
        'record' => [
            'remove' => 'Really delete record?'
        ]
    ],
    'autocomplete' => [
        'no-suggestions' => 'No suggestions'
    ],
    'form' => [
        'labels' => [
            'name' => 'Name',
            'surname' => 'Surname',
            'submit' => 'Save',
            'title' => 'Title',
            'quantity' => 'Quantity',
            'covers' => 'Book cover',
            'label_author' => 'Author',
            'upload-text' => 'Drag & drop images here',
            'books_count' => 'Publications count',
            'status' => 'Status',
            'available_quantity' => 'Available quantity ',
            'author' => [
                'full_name' => 'Author name'
            ]
        ]
    ],
    'breadcrumbs' => [
        'Management' => 'Management',
        'Add' => 'Add',
        'Update' => 'Update',
        'Book' => 'Book',
        'Author' => 'Author',
    ],
    'no-result' => 'Rentbook is empty. Please either add a book or an author.',
    'status' => [
        'available' => 'Available',
        'borrowed' => 'No longer available',

    ],
    'filter-bar' => [
        'filter-label' => 'Filter'
    ]

];
