require('./bootstrap');
window.Vue = require('vue');

import PageLogoHeader from './components/management/PageLogoHeader'
import ManagementIndex from "./components/management/ManagementIndex";
import BookForm from "./components/management/books/BookForm";
import AuthorForm from "./components/management/author/AuthorForm";
import Breadcrumbs from "./components/management/Breadcrumbs";
import ListRecords from "./components/management/ListRecords";
import moment from 'moment'

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('DD.MM.YYYY HH:mm')
    }
})

Vue.directive('tooltip', function(el, binding){
    $(el).tooltip({
        title: binding.value,
        placement: binding.arg,
        trigger: 'hover'
    })
})

Vue.prototype.__ = require('./VueTranslation/Translation').default.translate;

const app = new Vue({
    el: '#app',
    components: {
        'management-page-logo-header': PageLogoHeader,
        'management-index': ManagementIndex,
        'book-form': BookForm,
        'author-form': AuthorForm,
        'breadcrumbs': Breadcrumbs,
        'list-records': ListRecords
    },
});


