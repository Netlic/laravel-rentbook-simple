<?php

use Bookrent\Common\Controllers\ImageController;
use Bookrent\Management\Controllers\AuthorController;
use Bookrent\Management\Controllers\BookController;
use Bookrent\Management\Controllers\ManagementController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/management', ManagementController::class);

// Book management routes
Route::get('/management/book/add', [BookController::class, 'renderForm']);
Route::get('/management/book/update/{id}', [BookController::class, 'renderForm']);

Route::post('/management/book/add', [BookController::class, 'add']);
Route::get('/management/book/list', [BookController::class, 'list']);
Route::post('/management/book/borrow/{id}', [BookController::class, 'borrow']);
Route::post('/management/book/return/{id}', [BookController::class, 'return']);
Route::get('/management/book/borrowings/{id}', [BookController::class, 'getBorrowData']);
Route::delete('/management/book/delete/{id}', [BookController::class, 'delete']);
Route::post('/management/book/{bookId}/delete-cover/{id}', [BookController::class, 'deleteCover']);
Route::post('/management/book/update/', [BookController::class, 'update']);
Route::get('/management/book/', function() {
    return view('management.books.actions');
});
//

// Author management routes
Route::get('/management/author/add', [AuthorController::class, 'renderForm']);
Route::get('/management/author/update/{id}', [AuthorController::class, 'renderForm']);

Route::post('/management/author/add', [AuthorController::class, 'add']);
Route::get('/management/author/', function() {
    return view('management.author.actions');
});
Route::post('/management/author/update/', [AuthorController::class, 'update']);

Route::get('/management/author/list', [AuthorController::class, 'list']);
Route::get('/management/author/list-by-name/{name}', [AuthorController::class, 'listByName']);
//

//Image routes
Route::get('/bookcover/{id}/{fileId}', [ImageController::class, 'getBookCover']);
