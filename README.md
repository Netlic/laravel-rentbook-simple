#Rentbook

Simple management application for renting book business. This app is powered by [Laravel](https://laravel.com/) and [Vue.js](https://vuejs.org/)

## Requirements
 - PHP 7.4
 - Laravel 8 [prerequisites](https://laravel.com/docs/8.x)
 - node.js
 - npm
 - composer
 
## Installation
 1. `git clone git@gitlab.com:Netlic/laravel-rentbook-simple.git`
 2. `cd laravel-rentbook-simple`
 3. `composer install`
 4. `npm install`
 5. create `.env` from `.env.example`, setup database connection, development environment etc.
 6. create database in your db system named after the config value in the `.env` file, have in mind also your [database config](/config/database.php)
 7. generate application encryption key `php artisan key:generate`
 8. `npm run dev`
 
 ## Running application
  - start php server provided by laravel framework
    - navigate to project root
    - run `php artisan serve`
  - navigate your browser to `127.0.0.1:8000/management` and begin discovery
  - enjoy
  
## Localization
It is possible to switch language to slovak or english. Just do a little checkout in the project root:
- `git checkout sk`
- `git checkout en`
